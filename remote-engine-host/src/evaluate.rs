//! This module contains functions which evaluate commands
//! coming from the host or the engine.

use remote_engine_lib::InternalMessage;

/// Evaluates `command` obtained from the Client.
///
/// This function returns a [`InternalMessage`](remote_engine_lib::InternalMessage) if the command
/// requires special action by the Host.
///
/// If besides this possible special action an UCI command has to be sent to
/// the Engine, this is returned as the first part of the return value.
pub fn evaluate_client(command: &str) -> (Option<&str>, Option<InternalMessage>) {
    if command.starts_with("remote-quit-local\n") {
        (Some("stop\nisready\n"), Some(InternalMessage::Quit))
    } else {
        (Some(command), None)
    }
}

pub fn evaluate_engine(command: &str) -> Option<&str> {
    if command.starts_with("id ")
        || command.starts_with("option ") {
        // Suppress the `id` command, because
        None
    } else {
        Some(command)
    }
}