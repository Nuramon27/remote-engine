//! Contains lazliy compiled regular expressions

use regex::{Regex, RegexBuilder};
use lazy_static::lazy_static;

lazy_static!{
    pub static ref ID: Regex = RegexBuilder::new(r"^id\s*\n")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn quit() {
        assert!(
            ID.is_match("id\n")
        );
    }
}