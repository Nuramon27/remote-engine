#![deny(intra_doc_link_resolution_failure)]

mod evaluate;
mod regular_expressions;

use std::path::{Path, PathBuf};
use std::io::prelude::*;
use std::io::BufReader;
use std::process::{Command, Stdio, ChildStdout, ChildStdin};
use std::net::{TcpStream, TcpListener};

use structopt::StructOpt;
use crossbeam::{channel, channel::TryRecvError, scope, Receiver, Sender};
use get_if_addrs::get_if_addrs;

use remote_engine_lib::InternalMessage;

use evaluate::{evaluate_client, evaluate_engine};


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[derive(StructOpt)]
pub struct Opt {
    #[structopt(short = "d", long)]
    pub leela_dir: PathBuf,
    #[structopt(short = "l", long, default_value = "lc0")]
    pub leela_exec: PathBuf,
    #[structopt(long, default_value = "2980")]
    pub port: u32,
    #[structopt(long = "tb")]
    pub tablebases: Option<PathBuf>,
    #[structopt(short = "w", long)]
    pub weights: Option<PathBuf>,
}

/// Waits for input from `engine_stdout` and processes it line by line.
///
/// input is evaluated and forwarded to `stream` if necessary.
/// If a message is received on `rec`, an according action is taken.
fn await_engine(mut stream: TcpStream, mut engine_stdout: impl BufRead, rec: Receiver<InternalMessage>) -> Result<(), std::io::Error> {
    let mut buf = Vec::new();
    loop {
        engine_stdout.read_until(b'\n', &mut buf)?;
        let input = String::from_utf8_lossy(&buf);
        if let Some(to_forward) = evaluate_engine(&input) {
            stream.write_all(to_forward.as_bytes())?;
            stream.flush()?;
        }
        buf.clear();

        match rec.try_recv() {
            Ok(message) => match message {
                    InternalMessage::Quit => break Ok(()),
                },
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => panic!("Connection to main thread disconnected."),
        }
    }
}

/// Takes necessary actions in order to cleanly shut down the connection
/// to a client.
fn exit_connection(mut engine_stdin: impl Write, ser: &mut Sender<InternalMessage>) -> Result<(), std::io::Error> {
    // Send a message to quit down the channel
    ser.send(InternalMessage::Quit).expect("InternalMessage to subthread could not be sent.");
    // The thread processing output from the engine is currently waiting for output from it.
    // So we send the UCI command "isready" to the engine which makes it respond with "readyok",
    // so this thread will wake up and recieve the message just sent.
    writeln!(engine_stdin, "isready")
}

/// Waits for input from `stream` and processes it line by line.
///
/// Input is evaluated and forwarded to `engine_stdin` if necessary.
/// If a client action is indicated by the input, a notification is sent
/// via `ser`.
fn await_client(mut stream: TcpStream, mut engine_stdin: impl Write, ser: &mut Sender<InternalMessage>) -> Result<(), std::io::Error> {
    let mut stream_reader = BufReader::new(stream.try_clone()?);
    let mut buf = Vec::new();
    loop {
        stream_reader.read_until(b'\n', &mut buf)?;
        let input = String::from_utf8_lossy(&buf);
        let (to_forward, message) = evaluate_client(&input);
        if let Some(message) = message {
            ser.send(message).expect("InternalMessage to subthread could not be sent.");
        }
        if let Some(to_forward) = to_forward {
            write!(engine_stdin, "{}", to_forward)?;
        }
        if let Some(message) = message {
            stream.write_all(b"remote-quit-local-recieved\n")?;
            stream.flush()?;
            match message {
                InternalMessage::Quit => {
                    // See documentation for `exit_connection`.
                    writeln!(engine_stdin, "isready")?;
                    break Ok(())
                },
            }
        }
        buf.clear();
    }
}

// Waits concurrently for input from `stream` and `engine_stdin` and processes them.
fn connect_client_engine(
    stream: TcpStream,
    engine_stdin: &mut (impl Write + Send),
    engine_stdout: &mut (impl BufRead + Send)
) -> Result<(), std::io::Error>
{
    let (ser, rec) = channel::unbounded();
    scope(|s| -> Result<(), std::io::Error> {
        let leela_thread = {
            let stream_clone = stream.try_clone()?;
            let mut ser_clone = ser.clone();
            s.spawn(move |_| -> Result<(), std::io::Error> {
                match await_client(stream_clone, &mut *engine_stdin, &mut ser_clone) {
                    // If the connection with the client has been shut down ordinarily,
                    // all necessary actions to shut down the corresponding host processes
                    // have been taken, so no further action is necessary.
                    Ok(()) => Ok(()),
                    // If the connection has been shut down violently,
                    // we need to ensure that the host processes
                    // shut down correctly.
                    Err(_) => exit_connection(engine_stdin, &mut ser_clone)
                }
            })
        };

        let rec_clone = rec.clone();
        let server_thread = s.spawn(move |_| -> Result<(), std::io::Error> {
            await_engine(stream, engine_stdout, rec_clone)
        });
        server_thread.join().unwrap()?;
        leela_thread.join().unwrap()?;
        Ok(())
    }).expect("Scope could not be executed correctly.")
}

/// Reads lines from `engine_stdout` until one line begins with "uciok". Then returns.
///
/// All other lines are discarded
fn read_until_uciok(mut engine_stdout: impl BufRead) -> Result<(), std::io::Error> {
    let mut buf = Vec::new();
    loop {
        engine_stdout.read_until(b'\n', &mut buf)?;
        if buf.starts_with(b"uciok") {
            break Ok(());
        }
        buf.clear();
    }
}

/// Starts the engine Leela zero in a separate process
/// and returns handles to its stdin and stdout.
fn start_engine(dir: &std::path::Path, executable: &std::path::Path) -> Result<(ChildStdin, ChildStdout), std::io::Error> {
    let engine = Command::new(dir.join(executable))
        .current_dir(&dir)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    let engine_stdin = engine.stdin
        .expect("Could not obtain stdin from engine.");
    let engine_stdout = engine.stdout
        .expect("Could not obtain stdout from engine.");

    Ok((engine_stdin, engine_stdout))
}

/// Initializes the engine with input `engine_stdin` and output `engine_stdout`,
/// setting important options like the weights-file and
/// tablebase paths.
fn initialize_engine(
    mut engine_stdin: impl Write, mut engine_stdout: impl BufRead,
    tablebases: Option<&Path>, weights: Option<&Path>,
) -> Result<(), std::io::Error>
{
    writeln!(engine_stdin, "uci")?;
    read_until_uciok(&mut engine_stdout)?;
    if let Some(weights) = weights {
        writeln!(engine_stdin, "setoption name WeightsFile value {}", weights.display())?;
    }
    if let Some(tablebases) = tablebases {
        writeln!(engine_stdin, r"setoption name SyzygyPath value {}", tablebases.display())?;
    }
    Ok(())
}

pub fn run(args: Opt) -> Result<(), std::io::Error> {
    let (mut engine_stdin, engine_stdout) = start_engine(&args.leela_dir, &args.leela_exec)
        .expect("Engine could not be started.");
    let mut engine_reader = BufReader::new(engine_stdout);

    initialize_engine(
        &mut engine_stdin, &mut engine_reader,
        args.tablebases.as_deref(), args.weights.as_deref()
    )?;

    for address in get_if_addrs()? {
        println!("Adress: {}", address.ip());
    }
    let listener = TcpListener::bind(&format!("0.0.0.0:{}", args.port))
        .expect("Tcp connection coud not be established.");

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => if let Err(e) = connect_client_engine(stream, &mut engine_stdin, &mut engine_reader) {
                eprintln!("Error from connection: {}", e);
            },
            Err(e) => eprintln!("Failed connection: {}", e),
        }
    }
    Ok(())
}