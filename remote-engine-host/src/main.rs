#![deny(intra_doc_link_resolution_failure)]

use structopt::StructOpt;

use remote_engine_host::{Opt, run};

fn main() {
    let args = Opt::from_args();

    run(args).unwrap();
}
