#![deny(intra_doc_link_resolution_failure)]

mod evaluate;
mod message;
mod uci_options;
mod regular_expressions;

use std::io::prelude::*;
use std::io::{stdin, Stdin, stdout, BufReader};
use std::net::TcpStream;

use either::{Left, Right};
use crossbeam::{channel, channel::TryRecvError, scope, Receiver, Sender};

use remote_engine_lib::InternalMessage;

use evaluate::{evaluate_stdin, evaluate_stdin_initialize, evaluate_host};
use message::PreConnectionMessage;

/// Sends a list of commands `initial_commands` which has possibly
/// been issued before establishing the remote connection down the `stream`.
///
/// Returns false if one command ordered us to quit. Returns true otherwise.
fn apply_initial_commands(
    initial_commands: Vec<(Option<String>, Option<InternalMessage>)>,
    stream: &mut TcpStream
) -> Result<bool, std::io::Error>
{
    for (to_forward, message) in initial_commands {
        if let Some(input) = to_forward {
            stream.write_all(input.as_bytes())?;
            stream.flush()?;
        }
        if let Some(message) = message {
            match message {
                InternalMessage::Quit => return Ok(false)
            }
        }
    }
    Ok(true)
}

/// Waits for input from stdin and processes it line by line.
///
/// Input is evaluated and forwarded to `stream` if necessary.
/// If a client action is indicated by the input, a notification is sent
/// via `ser`.
fn await_gui(mut stream: TcpStream, handle_stdin: Stdin, ser: Sender<InternalMessage>) -> Result<(), std::io::Error> {
    let mut input = String::new();
    loop {
        handle_stdin.read_line(&mut input)?;
        let (to_forward, message) = evaluate_stdin(&input);
        if let Some(message) = message {
            ser.send(message)
                .expect("InternalMessage to subthread could not be sent.");
        }
        if let Some(input) = to_forward {
            stream.write_all(input.as_bytes())?;
            stream.flush()?;
        }
        if let Some(message) = message {
            match message {
                InternalMessage::Quit => break Ok(()),
            }
        }
        input.clear();
    }
}

/// Waits for input from `stream` and processes it line by line.
///
/// Input is evaluated and forwarded to `output` if necessary.
/// If a message is received on `rec`, an according action is taken.
fn await_host(stream: TcpStream, mut output: impl Write, rec: Receiver<InternalMessage>) -> Result<(), std::io::Error> {
    let mut buf = Vec::new();
    let mut stream_reader = BufReader::new(stream);
    loop {
        stream_reader.read_until(b'\n', &mut buf)?;
        let input = String::from_utf8_lossy(&buf);
        let (to_forward, message) = evaluate_host(&input);
        if let Some(input) = to_forward {
            write!(output, "{}", String::from_utf8_lossy(&input.as_bytes()))?;
        }
        if let Some(message) = message {
            match message {
                InternalMessage::Quit => (),
            }
        }
        buf.clear();
        match rec.try_recv() {
            Ok(message) => match message {
                    InternalMessage::Quit => break Ok(()),
                },
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => panic!("Connection to main thread disconnected."),
        }
    }
}

/// Waits concurrently for input from `stream` and from stdin
/// and processes them.
pub fn connect_stdin_host(stream: TcpStream, handle_stdin: Stdin) -> Result<(), std::io::Error> {
    scope(|s| -> Result<(), std::io::Error> {
        let (ser, rec) = channel::unbounded();
        let client_thread = {
            let stream = stream.try_clone()?;
            s.spawn(move |_| -> Result<(), std::io::Error> {
                await_host(stream, stdout(), rec)
            })
        };
        await_gui(stream, handle_stdin, ser)?;
        client_thread.join().unwrap()?;
        Ok(())
    }).expect("Scope could not be executed correctly.")
}

#[cfg(feature = "stockfish")]
mod consts {
    pub const ENGINE_NAME: &'static str = "Stockfish";
    pub const PORT: u32 = 2981;
}
#[cfg(not(feature = "stockfish"))]
mod consts {
    pub const ENGINE_NAME: &'static str = "Lc0";
    pub const PORT: u32 = 2980;
}

pub fn run() -> Result<(), std::io::Error> {
    let handle_stdin = stdin();
    let mut uci_command_buffer = Vec::new();
    let ip_address = {
        let mut input = String::new();
        loop {
            handle_stdin.read_line(&mut input)?;
            match evaluate_stdin_initialize(&input) {
                Left((to_forward, message)) => {
                    uci_command_buffer.push((to_forward.map(str::to_string), message));
                },
                Right(pre_connection_message) => match pre_connection_message {
                    PreConnectionMessage::UCI => {
                        println!("id name {} remote v{}", consts::ENGINE_NAME, env!("CARGO_PKG_VERSION"));
                        println!("id author Manuel Simon");
                        println!("{}", uci_options::UCI_OPTIONS);
                        println!("uciok");
                    },
                    PreConnectionMessage::IsReady => {
                        println!("readyok")
                    },
                    PreConnectionMessage::SetRemoteIP(ip) => { break ip.to_string(); },
                    PreConnectionMessage::StartAnalyzing((to_forward, message)) => {
                        uci_command_buffer.push((to_forward.map(str::to_string), message));
                        break "127.0.0.1".to_string();
                    }
                }
            }
            input.clear();
        }
    };
    let mut stream = TcpStream::connect(&format!("{}:{}", ip_address.trim(), consts::PORT))?;
    if !apply_initial_commands(uci_command_buffer, &mut stream)? {
        return Ok(());
    }

    println!("readyok");
    connect_stdin_host(stream, handle_stdin)
}