//! This module contains functions which evaluate commands
//! coming from the GUI or the host.

use either::Either;
use either::{Either::Left, Either::Right};

use remote_engine_lib::InternalMessage;

use super::message::PreConnectionMessage;
use crate::regular_expressions::*;

/// Evaluates the UCI command `command` obtained from the GUI before the network connection
/// has been established.
///
/// In that state, some commands have to be directly used by the client, especially
/// the "setoption" command that sets the remote IP-Adress. If such a command
/// is received, a `Right` value is returned with the corresponding message.
///
/// Other commands need to be passed to the engine and must be stored until the network connection
/// stands. These are deferred to [`evaluate_stdin_initialize`].
pub fn evaluate_stdin_initialize(command: &str) -> Either<(Option<&str>, Option<InternalMessage>), PreConnectionMessage<>> {
    if UCI.is_match(command) {
        Right(PreConnectionMessage::UCI)
    } else if ISREADY.is_match(command) {
        Right(PreConnectionMessage::IsReady)
    } else if let Some(remote_ip) = OPTION_REMOTE_IP.captures(command)
        .and_then(|a| a.name("value")) {
        Right(PreConnectionMessage::SetRemoteIP(remote_ip.as_str()))
    } else if START_ANALYZING_COMMAND.is_match(command) {
        Right(PreConnectionMessage::StartAnalyzing(evaluate_stdin(command)))
    } else {
        Left(evaluate_stdin(command))
    }
}

/// Evaluates the UCI command `command` obtained from the GUI.
///
/// This function returns a [`InternalMessage`](remote_engine_lib::InternalMessage) if the command
/// requires special action by the Client.
///
/// If besides this possible special action a command has to be sent to
/// the host, this is returned as the first part of the return value.
pub fn evaluate_stdin(command: &str) -> (Option<&str>, Option<InternalMessage>) {
    if QUIT.is_match(command) {
        (Some("remote-quit-local\n"), Some(InternalMessage::Quit))
    } else if OPTION_SYZYGY_PATH.is_match(command) {
        (None, None)
    } else {
        (Some(command), None)
    }
}

/// Evaluates `command` obtained from the Host.
///
/// This function returns a [`InternalMessage`](remote_engine_lib::InternalMessage) if the command
/// requires special action by the Client.
///
/// If besides this possible special action an UCI command has to be sent to
/// the GUI, this is returned as the first part of the return value.
pub fn evaluate_host(command: &str) -> (Option<&str>, Option<InternalMessage>) {
    if command.starts_with("remote-quit-local-recieved") {
        (None, None)
    } else {
        (Some(command), None)
    }
}