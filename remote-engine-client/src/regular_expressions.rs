//! Contains lazliy compiled regular expressions

use regex::{Regex, RegexBuilder};
use lazy_static::lazy_static;

lazy_static!{
    pub static ref QUIT: Regex = RegexBuilder::new(r"^quit\s*\n")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref UCI: Regex = RegexBuilder::new(r"^uci\s*\n")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref ISREADY: Regex = RegexBuilder::new(r"^isready\s*\n")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref OPTION_REMOTE_IP: Regex = RegexBuilder::new(r"^setoption\s+name\s+remoteip\s+value\s+(?P<value>.+?)\s*\n")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref OPTION_SYZYGY_PATH: Regex = RegexBuilder::new(r"^setoption \s+ name \s+ SyzygyPath")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref OPTION_GENERAL: Regex = RegexBuilder::new(r"^setoption\s+name\s+[^(remote-ip)]")
        .case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

lazy_static!{
    pub static ref START_ANALYZING_COMMAND: Regex = RegexBuilder::new(concat!(
        r"^go",
        r"|^ucinewgame",
        r"|^position",
        r"|^ponderhit",
    )).case_insensitive(true)
        .ignore_whitespace(true)
        .build()
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn quit() {
        assert!(
            QUIT.is_match("quit   \n")
        );
    }

    #[test]
    fn quit_carriage_return() {
        assert!(
            QUIT.is_match("quit\r\n")
        );
    }

    #[test]
    fn uci() {
        assert!(
            UCI.is_match("uci\n")
        );
    }

    #[test]
    fn isready() {
        assert!(
            ISREADY.is_match("isready\n")
        );
    }

    #[test]
    fn option_remote_ip() {
        assert_eq!(
            OPTION_REMOTE_IP.captures("setoPtion Name RemoteIp value 127.0.0.1\n")
                .and_then(|a| a.name("value"))
                .expect("Regex OPTION_REMOTE_IP does not work")
                .as_str(),
            "127.0.0.1"
        );
    }

    #[test]
    fn option_remote_ip_carriage_return() {
        assert_eq!(
            OPTION_REMOTE_IP.captures("setoPtion Name RemoteIp value 127.0.0.1\r\n")
                .and_then(|a| a.name("value"))
                .expect("Regex OPTION_REMOTE_IP does not work")
                .as_str(),
            "127.0.0.1"
        );
    }

    #[test]
    fn option_syzygy_path() {
        assert!(
            OPTION_SYZYGY_PATH.is_match("setoption name SyzygyPath value E:\\irgendwo \n")
        );
    }

    #[test]
    fn option_general() {
        assert!(
            OPTION_GENERAL.is_match("SetOption name num-cpus value 4\n")
        );
        assert!(
            !OPTION_GENERAL.is_match("setoption name remote-ip value 127.0.0.1\n")
        )
    }

    #[test]
    fn start_analyzing_command() {
        assert!(
            START_ANALYZING_COMMAND.is_match("go nodes 100\n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("gu\n")
        );
        assert!(
            START_ANALYZING_COMMAND.is_match("position FEn\n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("setoption name position\n")
        );
        assert!(
            START_ANALYZING_COMMAND.is_match("ucinewgame    \n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("setoption name RemoteIp value 127.0.0.1\n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("uci\n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("isready\n")
        );
        assert!(
            !START_ANALYZING_COMMAND.is_match("setoption name Ponder value false\n")
        );
    }
}