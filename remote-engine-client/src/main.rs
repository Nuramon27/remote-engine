#![deny(intra_doc_link_resolution_failure)]

use remote_engine_client::run;

fn main() {
    match run() {
        Ok(()) => (),
        Err(e) => panic!("Connection Could not be established: {}", e),
    }
}
