//! Contains messages only necessary for the client

use remote_engine_lib::InternalMessage;

/// A message that results from the processing of a UCI command
/// that is received before the remote connection has been established.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum PreConnectionMessage<'a> {
    UCI,
    IsReady,
    SetRemoteIP(&'a str),
    StartAnalyzing((Option<&'a str>, Option<InternalMessage>)),
}