#![deny(intra_doc_link_resolution_failure)]

//! The `lc_lib` crate contains helper structs for `lc-client`
//! and `lc-remote`.
//!
//! This is currently only the [`InternalMessage`] enum.

/// A message to be sent between different threads of execution.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum InternalMessage {
    /// Indicates that the Client wants to shut down.
    Quit,
}

/*#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum MessageToHost<'a> {
    UCI(&'a [u8]),
    Quit,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum MessageToClient<'a> {
    UCI(&'a [u8]),
    Quit,

}*/